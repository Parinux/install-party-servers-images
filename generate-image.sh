#!/bin/sh
# Copyright (C) 2019 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This is a program to install Debian to a block device like a USB key,
# microSD, HDD, SSD, etc

set -e

# TODO: make that configurable through the command line arguments
machine="lime1-a20"
debian_arch="armhf"

# Variables configurable through the command line arguments
image=""
size=""
extra_packages=""

# shared variables to simplify the code
loop="" # required for the cleanup "trap" function
mount_point="" # would to retrieve it from the system
# arrays
packages=""
base_packages="\
	apt-cacher-ng \
	bind9 \
	bridge-utils \
	build-essential \
	debconf \
	dnsmasq \
	dnsutils \
	git \
	hostapd \
	htop \
	iftop \
	iotop \
	ipcalc \
	isc-dhcp-server \
	linux-image-armmp-lpae \
	locales \
	man-db \
	nano \
	nfs-kernel-server \
	nginx-full \
	nmon \
	openssh-server \
	openvpn \
	rsync \
	screen \
	sudo \
	tcpdump \
	tftpd-hpa \
	tshark \
	vim \
"

usage()
{
    echo "$0 generate-full-image <path/to/loop/file> <size> [extra packages]"
    echo "$0 generate-unconfigured-image <path/to/loop/file> <size> [extra packages]"
    echo "$0 configure-image <path/to/loop/file> <size> [extra packages]"
    exit 1
}

umount_loop()
{
    loop="$1"

    if [ -b ${loop} ] ; then
	sudo umount "${loop}p1"
	udisksctl loop-delete -b ${loop}
    fi
    sync
}

cleanup()
{
    umount_loop "${loop}"
    exit 1
}

generate_base_image()
{
    image="$1"
    size="$2"

    rm -f "${image}"
    # We use qemu-img to create an image file way
    # faster than with ddrescue. This has doesn't
    # seem to create any issue even if the file
    # size is allocated on demand on the filesystem.
    # See what sparse files are for more information
    # on the topic
    qemu-img create -f raw "${image}" "${size}"

    # We add a single partition in the image file
    echo ';;L;*' | sfdisk "${image}"

    # we then use udisksctl to setup the loop device mapping
    # this is more convenient than losetup and can also work
    # without root permissions
    loop="$(udisksctl loop-setup --file ${image} | sed 's#^Mapped file .* as ##' | sed 's#\.$##')"

    # Create the filesystem
    sudo mkfs.ext4 "${loop}p1"
    # make sure that everything has been written before proceding on
    sync

    # we can't use udisksctl to mount the filesystem as it
    # will use nosuid and nodev, so we need instead to use
    # the normal mount with sudo
    mount_point="$(mktemp -d)"
    sudo mount "${loop}p1" "${mount_point}"

    # This will enable to chroot into the arm installation
    # transparently from an x86 computer, and enable
    # debootstrap to run the package configuration which
    # requires us to run arm code.
    sudo install -d -o root -g root -m755 "${mount_point}/usr/bin/"
    sudo cp -f /usr/bin/qemu-arm-static "${mount_point}/usr/bin/"

    # Add bootloaders and architecture specific packages
    if [ "$machine" = "lime1-a20" ] ; then
	packages="${packages} u-boot-sunxi"
    fi

    # Convert the list of packages into a format suitable
    # for debootstrap
    # for instance "openssh-server openvpn" becomes
    # "openssh-server,openvpn"
    debian_packages=""
    for package in ${packages} ; do
	debian_packages="${debian_packages},$package"
    done

    # remove the starting ',' at the beginning of the
    # debian_packages string because the following doesn't
    # look good: ",bind9,isc-dhcp-server,[...]" we want
    # "bind9,isc-dhcp-server,[...]" instead
    debian_packages="$(echo ${debian_packages} | sed 's#^,##')"

    # we use --foreign to do the installation in two passes
    # the first pass is just the extraction of the pakcages
    # the second pass configures the packages
    # this way it's probably faster as we can be sure that
    # qemu is used only in the second path
    # It also enables to do the second path on real hardware
    # in the future
    sudo /usr/sbin/debootstrap \
	 --arch "${debian_arch}" \
	 --foreign \
	 --include="${debian_packages}" \
	 stretch \
	 "${mount_point}" \
	 http://ftp.us.debian.org/debian

    sudo chroot "${mount_point}" /debootstrap/debootstrap --second-stage
}

configure_base_image()
{
    mount_point="$1"
    image="$2"

    # We then need to copy some configuration files
    # the install.sh script takes care of copying the
    # files in the rootfs located at "${mount_point}"
    # Rationale:
    # - The installation scripts are needed to take care
    #   of the files permissions. This is why the 'install'
    #   command is used inside them, as it supports file
    #   permissions while cp does not.
    # - The files that are machine independant are copied
    #   first, so the machine specific install script
    #   can override or delete them
    # Caveats:
    # - /!\ Don't forget to update the install script
    #   after adding a new file inside the configuration
    #   directories
    sudo configs/common/install.sh "${mount_point}" "${image}"
    sudo configs/pxe-install/install.sh "${mount_point}" "${image}"
    sudo configs/machines/${machine}/install.sh "${mount_point}" "${image}"

    # Enable the user to override some configurations as well
    # this could enable for instance to set a password that is not
    # pushed in git
    if [ -f configs/user/install.sh ] ; then
	sudo configs/user/install.sh  "${mount_point}" "${image}"
    fi
}

if [ $# -lt 3 ] ; then
    usage
fi

trap cleanup 0 1 2 3 6 15

# get the command line arguents
# first argument -> image
# second argument -> size
# the rest of the arguments -> extra_packages
action="$1"
image="$2"
size="$3"
shift 3
extra_packages="$@"
packages="${base_packages} ${extra_packages}"

set -x

if [ "${action}" = "generate-full-image" ] ; then
    generate_base_image "${image}" "${size}"
    configure_base_image "${mount_point}" "${image}"
elif [ "${action}" = "generate-unconfigured-image" ] ; then
    generate_base_image "${image}" "${size}"
elif [ "${action}" = "configure-image" ] ; then
    # Mount the image like we do in generate_base_image
    loop="$(udisksctl loop-setup --file ${image} | sed 's#^Mapped file .* as ##' | sed 's#\.$##')"
    mount_point="$(mktemp -d)"
    sudo mount "${loop}p1" "${mount_point}"

    # configure the image
    configure_base_image "${mount_point}" "${image}"
else
    usage
fi

generate_base_image "${image}" "${size}"
configure_base_image "${mount_point}" "${image}"

# At the end we still need to unmount the image
# file before being able to copy it to the device
# storage. umount_loop is in a function to also be
# called when the script goes into error or that the
# users stops it with ctrl+c
umount_loop "${loop}"

echo "The image is ready at: $(realpath ${image})"
echo "You can now install it with gnome-disks or ddrescue."
echo "There is more details on that in the README file."
