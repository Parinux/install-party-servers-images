#!/bin/sh
set -x
set -e

gadget="/sys/kernel/config/usb_gadget/otg"
mkdir -p ${gadget}
echo 0x0525 > ${gadget}/idVendor
echo 0xa4a7 > ${gadget}/idProduct

mkdir -p ${gadget}/configs/otg.1/

# Serial: ttyGS0
mkdir -p ${gadget}/functions/acm.usb0
if [ ! -e ${gadget}/configs/otg.1/acm.usb0 ] ; then
  ln -s ${gadget}/functions/acm.usb0 ${gadget}/configs/otg.1/acm.usb0
fi

if [ "$(cat ${gadget}/UDC)" != "" ] ; then
  echo '' > ${gadget}/UDC
fi
echo "$(ls /sys/class/udc/)" > ${gadget}/UDC
