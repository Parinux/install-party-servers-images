#!/bin/sh
destdir="$1"
image_file="$2"

thisdir="$(dirname $(realpath $0))"

# This contains the install_file function
source "${thisdir}/../../install_common.sh"

# boot configuration for the bootloader
install_file etc/kernel/postinst.d/zz-generate-dtb-link 755
install_file boot/extlinux/extlinux.conf

# install the bootloader
dd if="${destdir}/usr/lib/u-boot/A20-OLinuXino-Lime/u-boot-sunxi-with-spl.bin" \
	bs=1024 seek=8 conv=notrunc \
	of="${image_file}"

# IP static addresss
install_file etc/network/interfaces.d/lan0
install_file etc/network/interfaces.d/wan0

# USB device support
install_file usr/local/bin/usb-otg.sh 755
install_file etc/systemd/system/usb-otg.service
# Load the modules required for USB device support
echo 'configfs' >> "${destdir}/etc/modules-load.d/modules.conf"
echo 'libcomposite' >> "${destdir}/etc/modules-load.d/modules.conf"

# enable it to start at boot:
execute_in_chroot systemctl enable usb-otg.service

# enable systemd services for the virtual serial port over the USB device
# connector
execute_in_chroot systemctl enable serial-getty@ttyGS0.service

# This enables login on the emulated serial console that
# is available on the USB device port
# As we want to append to the file we use echo
echo '' >> "${destdir}/etc/securetty"
echo '# USB Gadget serial console(s)' >> "${destdir}/etc/securetty"
echo 'ttyGS0' >> "${destdir}/etc/securetty"

# Enable ipv4 forwarding for nat and routing
install_file etc/sysctl.d/forward.conf
install_file etc/iptables/rules.up
install_file etc/network/if-up.d/iptables 755

# Lime1 DNS settings
install_file etc/resolv.conf

# DNS server configuration
install_file etc/bind/named.conf
execute_in_chroot rndc-confgen -a -u  bind -A hmac-sha512
execute_in_chroot systemctl enable bind9

# DHCP configuration
install_file etc/dhcp/dhcpd.conf
install_file etc/systemd/system/dhcpd4.service
execute_in_chroot systemctl enable dhcpd4

# Only enable key based login for SSH
install_file etc/ssh/sshd_config

# Workaround for two issues:
# - linux-image-armmp-lpae currently installs the
#   linux-image-4.9.0-8-armmp-lpae package as a dependency, which is known
#   not to boot on a lime 1 A20:
#   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=922478
# - /etc/kernel/postinst.d/zz-generate-dtb-link is copied after the kenrel
#   is installed so the /dtb link is not created, which prevents the
#   device from booting as the devicetree which is required for booting
#   cannot be found
execute_in_chroot apt update
execute_in_chroot apt install linux-image-4.9.0-7-armmp-lpae
