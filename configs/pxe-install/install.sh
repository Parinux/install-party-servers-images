#!/bin/sh
destdir="$1"
image_file="$2"

thisdir="$(dirname $(realpath $0))"

# This contains the install_file function
source "${thisdir}/../install_common.sh"

install_dir opt/tftp/debian
install_dir opt/tftp/debian-testing
install_dir opt/tftp/kubuntu
install_dir opt/tftp/lubuntu
install_dir opt/tftp/pxelinux.cfg
install_dir opt/tftp/sysresccd
install_dir opt/tftp/ubuntu
install_dir opt/tftp/ubuntu-fr
install_dir opt/tftp/xubuntu
										     
install_dir opt/www/debian
install_dir opt/www/ubuntu
install_dir opt/www/iso

install_file etc/default/pxe-install

execute_in_chroot apt remove resolvconf

if [ ! -d ${destdir}/.git ] ; then
  git -C ${destdir} init
  git -C ${destdir} remote add origin https://git.framasoft.org/Parinux/pxe-install.git
fi
git -C ${destdir} fetch
git -C ${destdir} checkout origin/master -f

execute_in_chroot /opt/scripts/generate_dhcpd_config.sh 
execute_in_chroot /opt/scripts/generate_fake_dns_zone_for_dnsmasq.sh

# update isos
# This doesn't fit in 30G images, so it is disabled by default
# execute_in_chroot make -C /opt/scripts iso
