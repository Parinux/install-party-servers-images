install_dir()
{
        path="$1"
        permissions="$2"
        user="$3"
        group="$4"

        if [ "${permissions}" = "" ] ; then
            permissions="644"
        fi
        if [ "${user}" = "" ] ; then
                user="root"
        fi
        if [ "${group}" = "" ] ; then
            group="root"
        fi

        install -d "${destdir}/${path}"
}

install_file()
{
	path="$1"
	permissions="$2"
	user="$3"
	group="$4"

        if [ "${permissions}" = "" ] ; then
            permissions="644"
        fi
	if [ "${user}" = "" ] ; then
		user="root"
	fi
	if [ "${group}" = "" ] ; then
            group="root"
        fi

	install -d "${destdir}/$(dirname ${path})"
	install -t "${destdir}/$(dirname ${path})" \
		-o "${user}" \
		-g "${group}" \
		-m "${permissions}" \
		"${thisdir}/${path}"
}

install_link()
{
	link_to="$1"
	link_from="$2"

	install -d "${destdir}/$(dirname ${link_to})"
	ln -s -f -t "${destdir}/$(dirname ${link_to})" \
		${link_from} \
		${link_to}
}

execute_in_chroot()
{
    commands="$@"
    LANG=C.UTF-8 \
	PATH="/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin" \
	TERM=xterm-color \
	chroot "${destdir}" \
	sh -i -c "${commands}"
}
