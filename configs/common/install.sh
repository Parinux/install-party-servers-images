#!/bin/sh
destdir="$1"
thisdir="$(dirname $(realpath $0))"

# This contains the install_file function
source "${thisdir}/../install_common.sh"

install_file etc/apt/sources.list

# This sets up the clock to UTC
install_file etc/adjtime
execute_in_chroot dpkg-reconfigure tzdata

# This setups the locales
install_file etc/locale.gen
install_file etc/locale.conf
execute_in_chroot locale-gen
